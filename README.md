# Running your own hush lite server

This write up will explain how to setup your own light (lite) wallet server to use with [Hush's SilentDragonLite wallet](https://github.com/MyHush/SilentDragonLite) or [CLI version](https://github.com/MyHush/silentdragonlite-cli).

### Install & Setup your Linux server or VPS

Install your preferred distro. In this example I am using a VPS running Ubuntu 20.04, so these instructions should be very similar for Debian-based distros.

##### Pre Steps

1. Install GNU screen

    ```
    $ sudo apt install screen
    ```

1. Install the Go language and NGINX.

    ```
    $ sudo apt install golang nginx
    ```

##### Setup Hushd

1. Setup hushd user account with a root/sudo account

    - Create hush user account by ```useradd -r -m -s /bin/bash -d /home/hush hush```
    - Change the password with ```passwd hush```
    - Add user account hush to sudo list ```sudo visudo```

1. Start a screen session and change to user hush with ```sudo -u hush -s```

1. Download and setup the Hush daemon [hushd Version 3.x (choose latest release at link)](https://github.com/MyHush/hush3/releases)

    ```
    $ wget https://github.com/MyHush/hush3/releases/download/v3.5.2/hush-3.5.2-amd64.deb
    $ sudo dpkg -i hush-3.5.2-amd64.deb
    ```

1. This is my HUSH3.conf file located in ~/.komodo/HUSH3/HUSH3.conf. I would change the values below with CHANGETHIS appended and you can change the rpcport if you'd like:

    ```
    rpcuser=user-CHANGETHIS
    rpcpassword=pass-CHANGETHIS
    rpcport=18031
    server=1
    daemon=1
    txindex=1
    rpcworkqueue=256
    rpcallowip=127.0.0.1
    rpcbind=127.0.0.1

    addressindex=1
    spentindex=1
    timestampindex=1

    showmetrics=1

    addnode=explorer.myhush.org
    addnode=stilgar.leto.net
    addnode=dnsseed.bleuzero.com
    addnode=dnsseed.hush.quebec
    ```

1. Run hushd at the command line. You should see a bunch of text scrolling.

1. Then check if the Hush blockchain is downloading by noticing if the blockchain directory is increasing.

    ```
    $ du -h ~/.komodo/HUSH3/blocks/
    ```

1. The blockchain download will take some time, so open another terminal (or screen) and continue to install Lightwalletd.

##### Setup Lightwalletd

1. Then as user hush ```sudo -u hush -s``` download the [Hush Lightwalletd](https://gitlab.com/jahway603/lightwalletd)

    ```
    $ git clone https://gitlab.com/jahway603/lightwalletd
    ```

1. Get a TLS certificate. If you running a public-facing server, the easiest way to obtain a certificate is to use a NGINX reverse proxy and get a Let's Encrypt certificate. Since we're using Ubuntu here **I SUGGEST YOU DO NOT USE SNAPD** and just ```sudo apt install certbot``` and then start on [Step 7 of these instructions by the EFF](https://certbot.eff.org/instructions).

1. Create a new section for the NGINX reverse proxy:
   
    ```
    server {
        listen 443 ssl http2;
    
        ssl_certificate     ssl/cert.pem; # From certbot
        ssl_certificate_key ssl/key.pem;  # From certbot
        
        location / {
            # Replace localhost:9067 with the address and port of your gRPC server if using a custom port
            grpc_pass grpc://localhost:9067;
        }
    }
    ```

1. Edit the included shell script for your needs and then run the Lightwalletd frontend with:

    ```
    $ sudo ./hush-lightd.sh
    ```

1. You should start seeing the frontend ingest and cache the Hush blocks after ~15 seconds. Success!

##### Point the `silentdragonlite-cli` to this server

1. Now to test if it's working with a client by connecting to your server! Substitute your server below & note that I had to checkout dev branch as stable wasn't building for me, so I'd try either.
    
    ```
    $ git clone https://github.com/MyHush/silentdragonlite-cli
    $ cd silentdragonlite-cli
    $ git checkout origin/dev
    $ cargo build --release
    $ ./target/release/silentdragonlite-cli --server https://lite.myhush.org
    ```

1. Success!